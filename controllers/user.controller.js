const User = require('../models/user.model');
const gatewayService = require('../services/Gateway');
const jwt = require('jsonwebtoken');
const redis_client = require('../redis_connect');
const token = require('../middlewares/token');
const https = require('https');
const http = require('http');
const mulesoft = require('../services/mulesoft');

async function Register(req, res){

    // encrypt password
    const user = new User({
        username: req.body.username,
        password: req.body.password
    });

    try {
        const saved_user = await user.save();
        res.json({status: true, message: "Registered successfully.", data: saved_user});
    } catch (error) {
        // do logging in DB or file.
        res.status(400).json({status: false, message: "Something went wrong.", data: error});
    }
}

async function Login (req, res) {
    const username = req.body.username;
    const password = req.body.password;

    try {
        const user = await User.findOne({username: username, password: password}).exec();
        

        if(user === null) res.status(401).json({status: false, message: "username or password is not valid."});
        console.log('user', user);
        const access_token = jwt.sign({sub: user._id}, process.env.JWT_ACCESS_SECRET, { expiresIn: process.env.JWT_ACCESS_TIME});
        console.log('access_token', access_token);
        const refresh_token = GenerateRefreshToken(user._id);
        return res.json({status: true, message: "login success", data: {access_token, refresh_token}});
    } catch (error) {
        return res.status(401).json({status: true, message: "login fail", data: error});
    }

    
}

async function Logout (req, res) {
    const user_id = req.userData.sub;
    const token = req.token;

    // remove the refresh token
    await redis_client.del(user_id.toString());

    // blacklist current access token
    await redis_client.set('BL_' + user_id.toString(), token);
    
    return res.json({status: true, message: "success."});
}

function GetAccessToken (req, res) {
    const user_id = req.userData.sub;
    const access_token = jwt.sign({sub: user_id}, process.env.JWT_ACCESS_SECRET, { expiresIn: process.env.JWT_ACCESS_TIME});
    const refresh_token = GenerateRefreshToken(user_id);
    return res.json({status: true, message: "success", data: {access_token, refresh_token}});
}

function GenerateRefreshToken(user_id) {
    const refresh_token = jwt.sign({ sub: user_id }, process.env.JWT_REFRESH_SECRET, { expiresIn: process.env.JWT_REFRESH_TIME });
    
    redis_client.get(user_id.toString(), (err, data) => {
        if(err) throw err;

        redis_client.set(user_id.toString(), JSON.stringify({token: refresh_token}));
    })

    return refresh_token;
}
function StoreToken(user_id) {
  
    
    redis_client.get(user_id.toString(), (err, data) => {
        if(err) throw err;

        //redis_client.set(user_id.toString(), JSON.stringify({token: refresh_token}));
        redis_client.setex("auth_token", process.env.JWT_ACCESS_TIME, JSON.stringify(req.body.token), function(err, reply) {
            if (err) {
              return next(err)
            }
        
            console.log(` Auth Token stored`);
        
           // return next()
          })
    })

    return refresh_token;
}
async function GetTokenURL(req, res) {
    console.log('///////response/////////'+ req.body.user_id);
    console.log('inicia GetTokenURL');
    const tokenCache =null;
    try{
        tokenCache = await token.GetTokenCache(req.body.user_id);
        console.log('token cache:'+ tokenCache);
        if(tokenCache!=null) {
            response.access_token= tokenCache;
            res.status(201).send(tokenCache);
        }
        console.log("token cache:"+ tokenCache);

        await gatewayService.GetAccessTokenURL()
        .then(response => {
            
            response = JSON.parse(response);
            console.log("///////StoreCache/////////");
            let store_token = token.StoreCache(req.body.user_id,response.access_token);
            
            res.status(200).send(response);
            
          })
          .catch (error =>{
            console.log("hubo un error");
            console.log(error);
           
          })
    }
    catch(e){
        res.send(e);
    }

    
}

async function getPayRef (req, res) {
   
    // blacklist current access token
    await mulesoft.getPaymentReference(req,res)
    .then(response => {
            
        response = JSON.parse(response);
                
        res.status(201).send(response);
        
      })
      .catch (error =>{
        console.log("hubo un error");
        console.log(error);
        res.status(400).send("hubo un error: "+ error.data);
      })
    
   // return res.json({status: true, message: "success."});
}
  

module.exports = {
    Register,
    Login,
    Logout,
    GetAccessToken,
    StoreToken,
    GetTokenURL,
    getPayRef,
}