const axios = require('axios');
const https = require('https');
const http = require('http');

async function getPaymentReference(req,res) {
    return new Promise(function (resolve, reject) {
      console.log("//////getPaymentReference//////");  
      const client_id = req.body.client_id;
        const tok = req.body.token;
        const telco_id = req.body.telco_id;

    var responseBody = ''
    const options = {
        hostname: 'uat.mule.telecom.com.ar',  //
        port: 443,
        path: '/ccc-payment-reference-sapi-uat/api/paymentReference/'+telco_id, //2915336644',//?realm=/convergente
        method: 'GET',
        headers: {
            "Client_id": client_id,
            "Authorization": 'Bearer ' + tok
        }
        
  }
  const data = "grant_type=client_credentials" 
  
  //change to https for local testing
   const request = https.request(options, (res) => {
 
    res.on('data', (chunk) => {
      responseBody += chunk;
      console.log("body" + responseBody);
   });
   res.on('end', () => {
    console.log("end" + responseBody);
        resolve(responseBody)
      });
  });
  request.on('error', (e) => {
    console.error(e);reject(e)});
    request.write(data);
    request.end();
    })
  }
  module.exports = {
    getPaymentReference
}
