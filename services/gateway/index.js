//const { default: axios } = require('axios');
const https = require('https');
const token = require('../../middlewares/token');
const axios = require('axios');

/* async function GetAccessTokenURL (req, res,next){
  console.log("//////GetAccessTokenURL//////");
  var responseBody = ''
  const options = {
      //hostname: 'idpsesiont.telecom.com.ar', 
      hostname: 'idpsesionu.telecom.com.ar',
      port: 443,
      path: '/openam/oauth2/access_token?realm=/convergente&scope=openid',//?realm=/convergente
      method: 'POST',
      headers: {
       'content-type': 'application/x-www-form-urlencoded',
       //'Authorization': 'Basic ' + new Buffer('2f62d548-a90a-4c6e-888d-eaad239805ee' + ':' + 'Botjd7tVShNWlA7tomv7ZspIGktqQpeNPP-tx5TwHK-0bTzyj7XrRjV8qtWNz4p79TeKAjmkBN0CAsWjpTbhXw').toString('base64'),
        
       'Authorization': 'Basic ' + new Buffer('f290416b-ca43-420d-9ba7-1d3239759cf9' + ':' + 'PIijljyqJyWOHRVO8PrvOxidO1HffY_XxONV_vPpsCsGmvpDho4jxox35b6shcHnD2KcNXr94s3rQ1V_xz5nuQ').toString('base64'),
       
      }
      
}
const data = "grant_type=client_credentials" 

req = await https.request(options, (res) => {

res.on('data', (chunk) => {
    responseBody += chunk;
    
 });
res.on('end', () => {

      res.responseBody= responseBody;
      console.log("end"+res.responseBody);
      
    });
});
req.on('error', (e) => {
res.status(401).json({status: false, message: "Invalid request."});});
req.write(data);
req.end();
console.log("request"+req);
;
}
*/

function GetAccessTokenURL (req, res) {
    return new Promise((resolve, reject) => {
    var responseBody = ''
    const options = {
        //hostname: 'idpsesiont.telecom.com.ar', --testing
        hostname: 'idpsesionu.telecom.com.ar',  //
        port: 443,
        path: '/openam/oauth2/access_token?realm=/convergente&scope=openid',//?realm=/convergente
        method: 'POST',
        headers: {
         'content-type': 'application/x-www-form-urlencoded',
         //'Authorization': 'Basic ' + new Buffer('2f62d548-a90a-4c6e-888d-eaad239805ee' + ':' + 'Botjd7tVShNWlA7tomv7ZspIGktqQpeNPP-tx5TwHK-0bTzyj7XrRjV8qtWNz4p79TeKAjmkBN0CAsWjpTbhXw').toString('base64'),
          
         'Authorization': 'Basic ' + new Buffer('f290416b-ca43-420d-9ba7-1d3239759cf9' + ':' + 'PIijljyqJyWOHRVO8PrvOxidO1HffY_XxONV_vPpsCsGmvpDho4jxox35b6shcHnD2KcNXr94s3rQ1V_xz5nuQ').toString('base64'), //en env
         
        }
        
  }
  const data = "grant_type=client_credentials" 
  
  //change to https for local testing
  const req = https.request(options, (res) => {
 
 res.on('data', (chunk) => {
      responseBody += chunk;
  
   });
  res.on('end', () => {

        resolve(responseBody)
      });
  });
  req.on('error', (e) => {
    console.error(e);reject(e)});
  req.write(data);
  req.end();
    })
}
function GetTokenURL (req,res) {
  console.log("//////GetTokenURL//////");
  GetAccessTokenURL(req,res)
  .then(response => {
            
    response = JSON.parse(response);
    
    let store_token = token.StoreCache(req.headers.client_id,response.access_token);
    
    res.status(201).send(response);
    
  })
  .catch (error =>{
    console.log("hubo un error");
    console.log(error);
   
  })
}

async function GetAccessTokenAxios (req, res,next) {
  console.log("//////GetAccessTokenAxios//////");
  console.log("req.access_token: "+req.access_token);
  

  if (req.access_token  == '' || req.access_token == undefined || req.access_token == null){
// compose a buffer based on consumer key and consumer secret
let buff = new Buffer.from(
  //`${process.env.CONSUMER_KEY}:${process.env.CONSUMER_SECRET}`
  req.headers.client_id+':'+req.headers.client_secret
  );
  console.log("buff: "+buff );
// encode the buffer to a base64 string
let buff_data = buff.toString("base64");
console.log("buff_data: "+buff_data );
const data = "grant_type=client_credentials" ;
// send the request to the api.
let response = await axios
  .default({

    //ignore ssl validation
    httpsAgent: new https.Agent({
      rejectUnauthorized: false,
    }),
    method: 'POST', // request method.
    url:'https://idpsesionu.telecom.com.ar:443/openam/oauth2/access_token?realm=/convergente&scope=openid', // request url
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded', // request content type
      'Authorization': 'Basic ' + buff_data,//new Buffer('f290416b-ca43-420d-9ba7-1d3239759cf9' + ':' + 'PIijljyqJyWOHRVO8PrvOxidO1HffY_XxONV_vPpsCsGmvpDho4jxox35b6shcHnD2KcNXr94s3rQ1V_xz5nuQ').toString('base64'),//${buff_data}', // request auth string.
    },
    data: data, // request data
  })

  .catch (error =>{
    console.log("hubo un error");
    console.log(error);
    res.status(400).json({
      status:error.name,
      message: error.response.data
    } );
  })
// set the access token to the request object.
req.access_token = response.data.access_token;
    
let store_token = token.StoreCache(req.headers.client_id,req.access_token);
    
console.log("access_token: "+ req.access_token);

  }
  
//call the next function.
return next();

};

async function sendToMulesoft(req,res,next){
  console.log("//////sendToMulesoft//////");
  // obtain our access token from the request object
  let access_token = req.access_token;

  console.log("access_token: "+ access_token);
  let client_id =req.headers.client_id;
  console.log("client_id: "+ client_id);
  let nro_linea =req.body.nro_linea;
  console.log("nro_linea: "+ nro_linea);
  // compose the authentication string
  let auth = `Bearer ${access_token}`;
  
  // ngrok url: TODO
  let cbUrl = "";
  
  // compose the reference no and message ref.
  //let refNo = uniqueString().slice(0,14);
  //let msgRef = uniqueString().slice(0,14);
  
  // send the request to the api
  let response = await axios.default({
  
      // ignore ssl validation
      httpsAgent:new https.Agent({
          rejectUnauthorized:false
      }),
      method:'GET', // request method.
      url:"https://uat.mule.telecom.com.ar/ccc-payment-reference-sapi-uat/api/paymentReference/"+nro_linea, // request URL
      
      headers:{
          'client_id':nro_linea,
          'Authorization':auth // request auth string
      }
  })
  .catch (error =>{
    console.log("hubo un error al obtener ref de pago");
    console.log(error);

    res.status(400).json({
      status:error.name,
      message: error.response.data
    } )
  })
  
  console.log("referencia de pago: "+response.data.paymentReferenceNumber );
  return res.send({
      status: "ok",
      message:response.data // response
  });
}
function ping(req, res, next) {

  res.send('Ok');

  return next()

}
module.exports = {
    GetAccessTokenURL,
    GetTokenURL,
    GetAccessTokenAxios,
    sendToMulesoft,
    ping
}