const redis_client = require('../../redis_connect');

function store(req, res, next) {
 
    redis_client.setex("auth_token", process.env.JWT_ACCESS_TIME, JSON.stringify(req.body.token), function(err, reply) {
      if (err) {
        return next(err)
      }
  
      logger.info(`[${res.locals.id}] Auth Token stored`)
  
      return next()
    })
  }
  function GetTokenCache(req, res, next) {
    console.log("//////GetTokenCache//////");
    const client_id = req.headers.client_id;

    if(client_id === null) return res.status(401).json({status: false, message: "Invalid request."});
    try {
        console.log("entro a redis- client_id: "+ client_id);
        // verify if token is in store or not
        redis_client.get(client_id, (err, data) => {
            if(err) throw err;

            if(data !=null) {
              console.log("obtengo data" + data);
              //return res.status(200).json({status: false, message: "token existente", token: data});
              // set the access token to the request object.
              req.access_token = JSON.parse(data).token;
             
            } 
            next();
        })
    } catch (error) {
        return res.status(401).json({status: true, message: "error en GetTokenCache", data: error});
    }
}



   function StoreCache(user_id,newToken) {
    console.log("///////StoreCache/////////");
     redis_client.get(user_id.toString(), (err, data) => {
         if(err) throw err;

         redis_client.setex(user_id.toString(), process.env.APPLICATION_CONTEXT_TTL, JSON.stringify({token: newToken})); 

         console.log(` Auth Token stored` );
         console.log(newToken);
   
     })

     return newToken;
 }

  const middleware = {
    store,
    StoreCache,
    GetTokenCache
  }
  
  module.exports = middleware